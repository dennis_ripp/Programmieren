package de.ostfalia.prog.ws21;

import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.enums.Enums.Figuren;;

public class Spieler  {

	Figur schluempfe[];
	final int smurfCnt = 4;
	Farbe spielerFarbe;
	boolean isActive;

	public Spieler(Farbe farbe, boolean isActive){
		this.spielerFarbe = farbe;
		this.isActive = isActive;
		this.schluempfe = new Figur[smurfCnt];
		
		//erstellt die 4 schlümpfe pro spieler
		createSmurfs();
	}
	
	
	//erstellt die 4 schlümpfe pro spieler
	private void createSmurfs() {
		for (int i = 0; i < smurfCnt; i++) {
			schluempfe[i] = new Figur(this.spielerFarbe, Figuren.SCHLUMPF, i);
		}
	}
	
}
