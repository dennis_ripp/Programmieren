package de.ostfalia.prog.ws21;
import java.util.Scanner;

import de.ostfalia.prog.ws21.enums.Richtung;

public class IO {

	String args;
	
	public IO() {
		//noch leer
	}
	
	
	/**
	 * Liest Tastatur Input ein und vergleich mit allen Werten im Enum Richtung 
	 * @return gibt das Enum zurück wenn Wert übereinstimmt
	 */
	public Richtung takeRoute() {
		Scanner keyboard = new Scanner(System.in);
		this.args = keyboard.nextLine();	
		keyboard.close();
		
		for(int i = 0; i < Richtung.values().length; i++) {
			
			String buf = Richtung.values()[i].toString();
			
			if(buf.equals(args)) {
				return Richtung.values()[i];
			}
		}
			
		
		
		return null;	
	}
	
}
