package de.ostfalia.prog.ws21;

import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.enums.Enums.Figuren;;

public class Figur {

	Farbe figurFarbe;
	Figuren figurTyp;
	
	String nameIndex;
	String iD;
	
	int fieldNumber = 0;
	public boolean isZombie;
	boolean isNPC = true;
	
	
	public Figur(Farbe farbe, Figuren typ, int no){
		this.figurFarbe = farbe;
		this.figurTyp = typ;
		
		if(this.figurTyp == Figuren.SCHLUMPF || typ == Figuren.ZOMBIESCHLUMPF){
			this.isNPC = false;
			this.nameIndex = String.valueOf((char)('A' + no));
		}
		
		configFigur();

	}
	private void configFigur() {
		if(this.isNPC) {
			switch(this.figurTyp){
			case FLIEGE:
				this.iD = "Bzz";
				this.fieldNumber = 20;
				this.isZombie = true;
				break;
			case OBERSCHLUMPF:
				this.iD = "Doc";
				this.fieldNumber = 29;

				break;
			default:
				this.iD = this.figurTyp.name();
				break;
			}
		} else {
			this.iD = this.figurFarbe + "-" + this.nameIndex;
		}
	}
	
	
	public String toString() { 
		
		String isZombieStr = (isZombie?":Z":"");
		
		if(isNPC) {
			return this.iD + ":" + this.fieldNumber + isZombieStr;
		} else {
			return (this.figurFarbe + "-" + this.nameIndex + ":" + this.fieldNumber + isZombieStr);
		}
	}
}
