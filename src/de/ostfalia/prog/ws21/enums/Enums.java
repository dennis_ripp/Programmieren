package de.ostfalia.prog.ws21.enums;

public class Enums {
			
	public enum Spieler {
		  KEINSPIELER,
		  SPIELEREINS,
		  SPIELERZWEI,
		  SPIELERDREI,
		  SPIELERVIER
		}
	public enum Figuren {
		  SCHLUMPF,
		  ZOMBIESCHLUMPF,
		  FLIEGE,
		  OBERSCHLUMPF,
		  SCHLUMPFINE
		}
	public enum Felder {
		FELD,
		STARTFELD,
		TUBEROSE,
		FLUSS,
		PILZ,
		DOCSLABOR,
		DORF
	}
	
	public enum DiceSides {
		ONE,
		TWO,
		THREE,
		FOUR,
		FIVE,
		FLY,
		SCHLUMPFINE
		
	}	
}
	
