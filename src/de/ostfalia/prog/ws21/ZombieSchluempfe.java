package de.ostfalia.prog.ws21;

import de.ostfalia.prog.ws21.enums.Richtung;
import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.interfaces.IZombieSchluempfe;


public class ZombieSchluempfe implements IZombieSchluempfe {
	
	
	PlayField playfield;
	
	public ZombieSchluempfe(Farbe ... farben) {		
		playfield = new PlayField(null, farben);
	}
	
	public ZombieSchluempfe(String stellung, Farbe ... farben){
		playfield = new PlayField(stellung, farben);	
	}
	
	//bewegt die Figur um @param augenzahl in Richtung Konsoleneingabe
	public boolean bewegeFigur(String figurName, int augenzahl) {
		return moveFigure(figurName, augenzahl, null);
	}
	
	//bewegt die Figur um @param augenzahl in Richtung @param richtung
	public boolean bewegeFigur(String figurName, int augenzahl, Richtung richtung) {
		return moveFigure(figurName, augenzahl, richtung);
	}

	private boolean moveFigure(String figurName, int augenzahl,Richtung richtung) {
		Figur figur = playfield.returnPlayerByFigurName(figurName);
		
		if(figur.figurFarbe == playfield.zug.spielerFarbe) {

			for (int i = 0; i < augenzahl; i++) {
				Field actualField = playfield.fieldArray[figur.fieldNumber];

				actualField.isHere.remove(figur);
				if(actualField.isFork) {
					if(richtung == null) {
						goNextField(actualField, figur , new IO().takeRoute() == Richtung.ABZWEIGEN);
					} else {
						goNextField(actualField, figur , richtung == Richtung.ABZWEIGEN);
					}
				} else{
					goNextField(actualField, figur, false);
				}
				playfield.fieldArray[i].doFieldEvent(actualField.getType(), figur);
			}
			playfield.nextTurn();
			
			return true;
			}			
		return false;
	}
	
	
	//setzt die Feldnummer abhängig von @param abzweigen und fügt die @param figur der @param actualField Liste hinzu
	public void goNextField(Field actualField, Figur figur, boolean abzweigen) {
		figur.fieldNumber = actualField.next;

		if(abzweigen) {
			figur.fieldNumber = actualField.alternativePath;
		}

		actualField.isHere.add(figur);
	}

	//gibt die Feldnummer der Figur zurück
	public int getFeldnummer(String name) {
		return playfield.returnPlayerByFigurName(name).fieldNumber;
	}


	//gibt zurück ob eine Figur ein Zombie ist
	public boolean istZombie(String name) {
		return playfield.returnPlayerByFigurName(name).isZombie;
	}


	//gibt die Farbe die am Zug ist zurück
	public Farbe getFarbeAmZug() {
		return playfield.zug.spielerFarbe;
	}

	//gibt den Gewinner zurück
	public Farbe gewinner() {

		return checkForWinner();
	}

	//ermittelt den gewinner
	private Farbe checkForWinner() {
		int[] figurCnt = new int[playfield.maxSpielerAnzahl];
		
		//zählt den counter der jeweiligen Farbe hoch pro Spielerfigur mit Farbpräfix
		for(Figur spieler : playfield.fieldArray[36].isHere) {
			switch(spieler.figurFarbe) {
				case ROT: 
					figurCnt[Farbe.ROT.ordinal()]++;
					break;
				case BLAU: 
					figurCnt[Farbe.BLAU.ordinal()]++;
					break;
				case GELB: 
					figurCnt[Farbe.GELB.ordinal()]++;
					break;
				case GRUEN: 
					figurCnt[Farbe.GRUEN.ordinal()]++;
					break;
				
				default: break;
			}
		}
			
		//gibt die Gewinnerfarbe zurück wenn ein Element des Arrays = 4 ist
		for (int i = 0; i < figurCnt.length; i++) {
			if (figurCnt[i] == 4) {
				playfield.gewinner = Farbe.values()[i];
				return Farbe.values()[i];
			}
		}	
		return null;
	}
	
	
}
