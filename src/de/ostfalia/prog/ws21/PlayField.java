package de.ostfalia.prog.ws21;

import de.ostfalia.prog.ws21.enums.Farbe;

import java.util.ArrayList;
import java.util.List;

import de.ostfalia.prog.ws21.enums.Enums.Figuren;;


public class PlayField {

	public final int fieldCount = 37;
	public final int maxSpielerAnzahl = 4;
	public int cnt = 0;
	public Field[] fieldArray;
	public Spieler[] spieler;
	public Spieler zug;
	public int activeSpielerAnzahl;
	public Figur[] npcs;
	public List<Spieler> activePlayer;
	public List<Spieler> turnList;
	public Farbe gewinner;
	
	
	
	public PlayField(String stellung, Farbe ... farben)
	{
		activePlayer = new ArrayList<Spieler>();
		turnList = new ArrayList<Spieler>();
		
		this.fieldArray = new Field[fieldCount];
		this.activeSpielerAnzahl = farben.length;
		this.spieler = new Spieler[maxSpielerAnzahl];
		this.npcs = new Figur[Figuren.values().length -2];
		
		String buf = stellung;
		
		//alle Felder erstellen
		createFields();
		
		//alle Spieler erstellen
		createPlayers();
		
		//alle NPCs erstellen
		createNPCs();
		
		//setzt die StartPosition
		setStartPosition(this.fieldArray);

		//deaktiviert Spieler, dessen Farben nicht als Parameter mitgegeben wurde
		activatePlayers(farben);
		
		//setzt die FeldNummer der inactiven Spieler auf -1
		setUnactivePlayersFieldNo();

		//setzt die Figuren gemäß dem Parameter Stellung
		setStellung(buf);
		
		//fügt die aktiven Spieler der aktiven Spieler Liste hinzu
		addActivePlayerToList();
		
		//sortiert die Spieler in eine Liste nach Reihenfolge der mitgegebenen Farben
		setOrderOfTurns(farben);
			
		//setzt den Startzug auf den Spieler der Turnliste 0tes Element
		this.zug = turnList.get(0);
	}

	private void setOrderOfTurns(Farbe... farben) {
		for (int i = 0;  i < farben.length; i++) {
			turnList.add(returnFigureByColor(farben[i]));
		}
	}

	private void addActivePlayerToList() {
		for (int i = 0; i < this.spieler.length; i++) {
			if(this.spieler[i].isActive) {
				activePlayer.add(spieler[i]);
			}
		}
	}

	private void setStellung(String buf) {
		if(buf != null && !buf.equals("")){
			String[] buffArr = buf.split(", ");
			
			List<String> buffList = new ArrayList<String>(); 
			
			for (int i = 0; i < buffArr.length; i++) {
				buffList.add(buffArr[i]);
			}
			
			for (String str : buffList) {
				setStartPosition(str);
			}
		}
	}
	
	//Setzt die Startposition gemäß dem Parameter "Stellung" 
	public void setStartPosition(String figurStr) {
		String[] buff =  figurStr.split(":");
		Figur buffFigur = returnPlayerByFigurName(buff[0]);
		int fieldNo = Integer.parseInt(buff[1]);
		
		this.fieldArray[fieldNo].isHere.add(buffFigur);
		buffFigur.fieldNumber = fieldNo;
	}
	
	//Fügt der isHere List der Feldern die Schlümpfe der aktiven Spieler hinzu
	public void setStartPosition(Field[] field) {
		for (Spieler currentPlayer : this.spieler) {
			if (currentPlayer.isActive) {
				for(int i = 0; i < currentPlayer.schluempfe.length; i++) {
						field[currentPlayer.schluempfe[i].fieldNumber].isHere.add(currentPlayer.schluempfe[i]);
				}
			}
		}
	}
	
	//erhöht den Zähler und wechselt so den Zug alternierend zu den aktiven Spielern
	public void nextTurn() {
		cnt++;
		this.zug = this.turnList.get(cnt % this.activeSpielerAnzahl);
	}

	//erstellt alle Felder
	private void createFields() {
		//create each field for the for the playfield ( i = 0-36 )
		for(int i = 0; i < fieldCount; i++) {
			this.fieldArray[i] = new Field(i);
		}
	}


	//erstellt alle Spieler und setzt die Aktivität erstmal auf false
	private void createPlayers() {
		//create Players by the amount of given colors
		boolean Active = false;
		for (int i = 0; i < this.maxSpielerAnzahl; i++) {
			this.spieler[i] = new Spieler(Farbe.values()[i], Active);
		}
	}	

	//erstellt alle NPCs die im Enum vorhanden sind
	private void createNPCs() {
		for (int i = 0; i < Figuren.values().length - 2; i++) {
			this.npcs[i] = new Figur(null, Figuren.values()[2+i], 0);
		}
	}
	
	//durchsucht die IDs aller Spieler und NPCs nach dem @param figurName
	public Figur returnPlayerByFigurName(String figurName){
		if(returnFigureByName(figurName) != null) {
			return returnFigureByName(figurName);
		}
			
		if(returnNPCByFigurName(figurName) != null) {
			return returnNPCByFigurName(figurName);
		}
		
		System.out.println("No figure found");	
		
		return null;
	}

	//durchsucht die IDs aller Spieler nach dem @param figurName
	private Figur returnFigureByName(String figurName) {
		//nested loop to look for schlumpf in each player with matching id
		for (int i = 0; i < this.spieler.length; i++) {
			for(int j = 0; j < this.spieler[i].schluempfe.length; j++) {
				if (this.spieler[i].schluempfe[j].iD.equals(figurName)) {
					return this.spieler[i].schluempfe[j];
				}
			}
		}
		
		return null;
	}
	
	//gibt das Spielerobjekt zurück welches mit der @param farbe übereinstimmt
	private Spieler returnFigureByColor(Farbe farbe ) {
		//nested loop to look for schlumpf in each player with matching id
		for (int i = 0; i < this.activePlayer.size(); i++) {
				if (this.activePlayer.get(i).spielerFarbe == farbe) {
					return this.activePlayer.get(i);
				}
		}
		return null;
	}

	//durchsucht die IDs aller NPCs nach dem @param figurName
	private Figur returnNPCByFigurName(String figurName) {
		//looking for npcs with matching id
		for (int i = 0; i < this.npcs.length; i++) {
			if (this.npcs[i].iD.equals(figurName)) {
				return this.npcs[i];
			}
		}
		return null;
	}

	//setzt die FeldNummer der inactiven Spieler auf -1
	private void setUnactivePlayersFieldNo() {
		for (Spieler currerntSpieler : this.spieler) {
			if (!currerntSpieler.isActive) {
				for(int i = 0; i < currerntSpieler.schluempfe.length; i++) {
						currerntSpieler.schluempfe[i].fieldNumber = -1;
				}
			}
		}
	}
	//aktiviert alle Spieler, dessen @param farbes mitgegeben wurde
	private void activatePlayers(Farbe ... farbes) {
		for (Spieler currentSpieler : this.spieler) {
			for(int i = 0; i < farbes.length; i++) {
				if(currentSpieler.spielerFarbe == farbes[i]) {
					currentSpieler.isActive = true;
				}
			}
		}
	}
}
