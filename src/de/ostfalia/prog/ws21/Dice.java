package de.ostfalia.prog.ws21;

import de.ostfalia.prog.ws21.enums.Enums.DiceSides;


public class Dice {

	final int diceSide = DiceSides.values().length;
	
	
	
	//calculate random number between Min = 0 (index starts at 0) and DiceSide which is the length of the enum 
	
	public int returnRandomSide() {
		
		int min = 0;
		int max = diceSide - 1;
		
		
		return min + (int)(Math.random() * ((max - min) + 1));
	}
	
	
	// Return Side/Eyes
	
	public DiceSides rollDice() {

		System.out.println(DiceSides.values()[returnRandomSide()] + " gewuerfelt");
		
		return DiceSides.values()[returnRandomSide()];
	}
	
	
}
